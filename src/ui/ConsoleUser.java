package ui;

import domain.*;
import service.SuperService;
import service.validators.ValidationException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

public class ConsoleUser implements Console {

    private final SuperService service;
    private final User loggedUser;
    BufferedReader reader;

    public ConsoleUser(SuperService service, User user) {
        this.service = service;
        this.loggedUser = user;
        reader = new BufferedReader(new InputStreamReader(System.in));
    }

    private void commands() {
        System.out.print(
                "addFriend\n"+"removeFriend\n"+
                "friends\n"+ "friendsSince\n"+
                "friendRequests\n"+
                "sendReqest\n"+
                "conv\n"+"messages\n"+
                 "sendmsj\n"+"replyAll\n"+ "help\n");
    }

    private void requestsCommands(){
        System.out.println(
                "acceptRequest\n"+ "declineRequest\n"
        );
    }

    @Override
    public void run(){
        commands();
        reader = new BufferedReader(new InputStreamReader(System.in));
        while(true){

            try {
                String comand= reader.readLine();
                switch (comand) {
                    case "addFriend" -> addFriend();
                    case "removeFriend" -> removeFriend();
                    case "friends" -> showFriends();
                    case "friendsSince" -> showFriendsSince();
                    case "help" -> commands();
                    case "friendRequests" -> showFriendRequests();
                    case "acceptRequest" -> acceptRequest();
                    case "declineRequest" -> declineRequest();
                    case "sendRequest" -> sendRequest();
                    case "conv" -> showConversations();
                    case "messages" -> showMessages();
                    case "sendmsj" -> sendMessage();
                    case "replaymsj" -> repalyMessage();
                    case "replyAll" -> replyAll();
                }
                if(comand.equals("exit"))
                    break;
            } catch (RuntimeException e) {
                System.out.println(e.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private void repalyMessage() throws IOException {
        System.out.println("Introduce your text (on a line):");
        String text = reader.readLine();
        System.out.println("Introduce message id to replay to:");
        String msjId= reader.readLine();
        System.out.println("Introduce email of user to send message to:");
        List<String> emails= Arrays.asList(reader.readLine().split(" "));

        Message newMessage= new Message(loggedUser.getId(),emails,text, LocalDate.now());
        ReplyMessageDTO newReplyMessageDTO= new ReplyMessageDTO(newMessage,msjId);
        service.replyMessage(newReplyMessageDTO);
        System.out.println("Message has been sent!");
    }

    private void sendMessage() throws IOException {
        System.out.println("Introduce your text (on a line):");
        String text = reader.readLine();
        System.out.println("Introduce email(s) to send messages to (separated by one space):");
        List<String> emails= Arrays.asList(reader.readLine().split(" "));

        Message newMessage= new Message(loggedUser.getId(),emails,text, LocalDate.now());
        service.sendMessage(newMessage);
        System.out.println("Message has been sent!");
    }

    private void showMessages() throws IOException {
        System.out.println("Introduce email to show messages with:");
        String email = reader.readLine();
       List<ReplyMessage> messages= service.getMessages(loggedUser.getId(),email);

        if(messages.size()>0){
            System.out.println("Your messages with "+ email+ " are:");


            messages.stream().forEach(message -> {
                if(message.getOriginal()==null) {
                    if (!message.getFrom().equals(loggedUser.getId()))
                        System.out.println("(" + message.getId() + ")" + message.getMessage());
                    if (message.getFrom().equals(loggedUser.getId()))
                        System.out.println("              " + "(" + message.getId() + ")" + message.getMessage());
                }
                else{
                    if(!message.getFrom().equals(loggedUser.getId())){
                        System.out.println();
                        System.out.println(".--> "+message.getOriginal().getMessage());
                        System.out.println("'-- ("+message.getId()+")"+message.getMessage());
                    }
                    else if(message.getFrom().equals(loggedUser.getId())){
                        System.out.println();
                        System.out.println("              .-->"+message.getOriginal().getMessage());
                        System.out.println("              '-- ("+message.getId()+")"+message.getMessage());
                    }

                }
            });
        }
        else System.out.println("You have no messages with "+email);
    }


    private void sendRequest() throws IOException {
        System.out.println("Introduce email to send request to:");
        String email = reader.readLine();

        service.sendRequest(loggedUser.getId(),email);
        System.out.println("Request has been sent!");
    }

    private void declineRequest() throws IOException {
        System.out.println("Introduce email of the request to be decline:");
        String email = reader.readLine();

        service.declineRequest(loggedUser.getId(),email);
        System.out.println("Request deleted!");
    }

    private void acceptRequest() throws IOException {
        System.out.println("Introduce email of the request to be accepted:");
        String email = reader.readLine();

        service.acceptRequest(loggedUser.getId(),email);
        System.out.println("Request accepted");
    }

    private void showFriendRequests() {

        Iterable<UserDTO> rez = service.getRequests(loggedUser.getId());
        if (!rez.iterator().hasNext()) {
            System.out.println("You have no new friend requests!");
        } else {
            rez.forEach(System.out::println);
            requestsCommands();
        }
    }

    private void showConversations() {
        Iterable<String>cov =service.getAllConversation(loggedUser.getId());
        if(!cov.iterator().hasNext()){
            System.out.println("You have no conversation!");
        }
        else {
            System.out.println("You have conversations with:");
            cov.forEach(System.out::println);
        }
    }

    private void showFriends() throws IOException {

        Iterable<FriendShipDTO> rez=service.getFriends(loggedUser.getId());
        if(!rez.iterator().hasNext()){
            System.out.println("You have no friends yet!");
        }
        else {
            System.out.println("Your friends are:");
            rez.forEach(us -> System.out.println(us.getUser2()));
        }
    }

    private void showFriendsSince() throws IOException {

        System.out.println("Year and month(yyyy-MM): ");
        String date = reader.readLine();
        YearMonth date1;
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM");
            date1 = YearMonth.parse(date, formatter);
        }
        catch (DateTimeParseException e){
            throw new ValidationException("Date time is invalid!");
        }
        Iterable<FriendShipDTO> rez = service.getFriendsSince(loggedUser.getId(), date1);
        if(!rez.iterator().hasNext()){
            System.out.println("This user has no friends from this date!");
        }
        else {
            System.out.println("Your friends sice " + date + " are:");
            rez.forEach(us -> System.out.println(us.getUser2() + " since " + us.getDate()));
        }
    }
    private void removeFriend() throws IOException {

        System.out.println("Email(s) of the user(s) to remove separated by one space: ");
        List<String> ids= Arrays.asList(reader.readLine().split(" "));
        service.removeFriend(loggedUser.getId(),ids);
        System.out.println("Friend(s) removed with success");
    }

    private void addFriend() throws IOException {
        System.out.println("Emails(s) of the user(s) to add as friend(s) separated by one space: ");
        List<String> ids= Arrays.asList(reader.readLine().split(" "));
        service.addFriend(loggedUser.getId(),ids);
        System.out.println("Friend(s) added with success");
    }

    private void replyAll() throws IOException{
        System.out.println("Introduce your text (on a line):");
        String text = reader.readLine();
        System.out.println("Introduce message id to replay to:");
        String msjId= reader.readLine();
        Message newMessage= new Message(loggedUser.getId(), null, text, LocalDate.now());
        ReplyMessageDTO replyMessageDTO= new ReplyMessageDTO(newMessage, msjId);
        service.replayAll(replyMessageDTO);
    }
}
