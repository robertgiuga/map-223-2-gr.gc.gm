package ui;

import domain.User;
import service.SuperService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SuperConsole implements Console{

    SuperService service;
    BufferedReader reader;


    public SuperConsole(SuperService service) {
        this.service = service;
        reader = new BufferedReader(new InputStreamReader(System.in));
    }
    private void comenzi() {
        System.out.print(
                "login\n"+"exit\n");
    }
    public void run(){

        while (true) {
            comenzi();
            try {
                String comand= reader.readLine();
                switch (comand){
                    case "login":
                        Console console = logIn();
                        console.run();
                        break;


                }
                if(comand.equals("exit"))
                    break;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (RuntimeException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private Console logIn() throws IOException {
        System.out.println("Please introduce your email: ");
        String id= reader.readLine();
        System.out.println("Please introduce your password: ");
        String passwd= reader.readLine();

        User user= service.logIn(id,passwd);
        if(user.isAdmin())
            return new ConsoleAdmin(service);
        else{
            return new ConsoleUser(service,user);
        }

    }
}
