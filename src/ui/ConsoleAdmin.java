package ui;

import domain.FriendShipDTO;

import domain.User;
import service.SuperService;
import service.validators.ValidationException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

public class ConsoleAdmin implements Console{
    SuperService service;
    BufferedReader reader;

    public ConsoleAdmin(SuperService service) {
        this.service = service;
        reader = new BufferedReader(new InputStreamReader(System.in));
    }

    /**
     * adds a new user
     * @param admin specify if a user is admin(true) or not(false)
     * @throws IOException
     */
    private void addUserOrAdmin(boolean admin) throws IOException{
        System.out.println("First Name:");
        String firstName= reader.readLine();
        System.out.println("Last Name:");
        String lastName=reader.readLine();
        System.out.println("Email:");
        String email=reader.readLine();
        System.out.println("Password:");
        String password= reader.readLine();
        User newUser = new User(firstName,lastName,email,password);
        if (admin)
            service.addAdmin(newUser);
        else
            service.addUser(newUser);
        System.out.println("User added with success");
    }
    private void removeUser()throws IOException{
        System.out.println("Email:");
        String id= reader.readLine();
        service.removeUser(id);
        System.out.println("User removed with success");
    }
    private void allUsers(){
        service.users().forEach(usr-> System.out.println(usr.getId()+" "+usr.toString()));
    }


    private void comenzi() {
        System.out.print("allUsers\n" +
                "addUser\n"+"addAdmin\n"+"removeUser\n"+"addFriend\n"+"removeFriend\n"+"nrCommunities\n"+"nrCommunities\n"
                +"socialCommunity\n"+
                "friends\n"+ "friendsSince\n"+"help\n");
    }

    private void showFriends() throws IOException {
        System.out.println("User's email: ");
        String id= reader.readLine();
        Iterable<FriendShipDTO> rez=service.getFriends(id);
        if(!rez.iterator().hasNext()){
            System.out.println("This user has no friends!");
        }
        else {
            System.out.println("Friends of " + id + " are:");
            rez.forEach(us -> System.out.println(us.getUser2()));
        }
    }

    private void showFriendsSince() throws IOException {
        System.out.println("User's email: ");
        String id= reader.readLine();
        System.out.println("Year and month(yyyy-MM): ");
        String date = reader.readLine();
        YearMonth date1;
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM");
            date1 = YearMonth.parse(date, formatter);
        }
        catch (DateTimeParseException e){
            throw new ValidationException("Date time is invalid!");
        }
        Iterable<FriendShipDTO> rez = service.getFriendsSince(id, date1);
        if(!rez.iterator().hasNext()){
            System.out.println("This user has no friends from this date!");
        }
        else {
            System.out.println("Friends of " + id + " since " + date + " are:");
            rez.forEach(us -> System.out.println(us.getUser2() + " since " + us.getDate()));
        }
    }

    private void socialCommunity() {
        service.socialCommunity().forEach(usr-> System.out.println(usr));
    }

    private void nrCommunities() {
        System.out.println("Nr of communities: "+ service.nrCommunities());
    }

    private void removeFriend() throws IOException {
        System.out.println("User's email to remove friend(s): ");
        String id= reader.readLine();

        System.out.println("Email(s) of the user(s) to remove separated by one space: ");
        List<String> ids= Arrays.asList(reader.readLine().split(" "));
        service.removeFriend(id,ids);
        System.out.println("Friend(s) removed with success");
    }

    private void addFriend() throws IOException {
        System.out.println("User's email to add friend(s): ");
        String id= reader.readLine();

        System.out.println("Emails(s) of the user(s) to add as friend(s) separated by one space: ");
        List<String> ids= Arrays.asList(reader.readLine().split(" "));
        service.addFriend(id,ids);
        System.out.println("Friend(s) added with success");
    }

    @Override
    public void run(){
        comenzi();
        while(true){

            try {
                String comand= reader.readLine();
                switch (comand) {
                    case "allUsers":
                        allUsers();
                        break;
                    case "addUser":
                        addUserOrAdmin(false);
                        break;
                    case "removeUser":
                        removeUser();
                        break;
                    case "addFriend":
                        addFriend();
                        break;
                    case "removeFriend":
                        removeFriend();
                        break;
                    case "nrCommunities":
                        nrCommunities();
                        break;
                    case "socialCommunity":
                        socialCommunity();
                        break;
                    case "friends":
                        showFriends();
                        break;
                    case "friendsSince":
                        showFriendsSince();
                        break;
                    case "help":
                        comenzi();
                        break;
                    case "addAdmin":
                        addUserOrAdmin(true);
                        break;
                }
                if(comand.equals("exit"))
                    break;
            } catch (RuntimeException e) {
                System.out.println(e.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }



}
