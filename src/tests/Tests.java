package tests;

import tests.RepositoryTest.RepoDBTest.FriendshipRequestDBTest;
import tests.RepositoryTest.RepositoryTests;
import tests.DomainTests.DomainTests;


public class Tests {

    public static void RunALL(){
        RepositoryTests.runTests();
        DomainTests.runTest();
        ServiceTests.runTests();
        FriendshipRequestDBTest.runTests();
    }
}
