package tests.RepositoryTest;

import domain.Friendship;
import tests.RepositoryTest.RepoDBTest.FriendshipDBTest;
import tests.RepositoryTest.RepoDBTest.MessageDBTest;
import tests.RepositoryTest.RepoDBTest.UserDBTest;

public class RepositoryTests {

    public static void runTests(){
       UserDBTest.runTests();
       FriendshipDBTest.runTests();
        MessageDBTest.runTests();
    }
}
