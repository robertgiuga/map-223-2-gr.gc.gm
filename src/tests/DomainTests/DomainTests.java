package tests.DomainTests;

import domain.Message;
import domain.User;

public class DomainTests {

    /**
     * run all domain tests
     */
    public static void runTest(){
        EntityTests.runTests();
        UserTest.runTests();
        FriendshipTests.runTests();
        TupleTests.runTest();
        TupleOneTests.runTest();
        MessageTests.runTest();
        ReplyMessageTests.runTest();
        UserDTOTests.runTests();
        FriendshipDTOTests.runTest();
    }

}
