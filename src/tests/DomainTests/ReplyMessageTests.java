package tests.DomainTests;

import domain.Message;
import domain.ReplyMessage;
import domain.User;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ReplyMessageTests {

    private ReplyMessageTests(){}

    public static void runTest()
    {
        testGetSet();
    }

    private static void testGetSet()
    {

        User user = new User("Gulea", "Cristian","gulea@gmail.com","a",false);
        User user1 = new User("Paul", "Marian","marian@gmail.com","a1",false);
        User user2 = new User("George", "Mihai","mihai@gmail.com","a2",false);
        List<String> list = new ArrayList<>(); list.add("marian@gmail.com"); list.add("mihai@gmail.com");
        List<String> list1 = new ArrayList<>(); list.add("gulea@gmail.com"); list.add("marian@gmail.com");
        LocalDate date = LocalDate.of(2020, 1, 8);
        Message message =  new Message( "mihai@gmail.com", list1, "Atentie!", date);
        message.setId(43);
        ReplyMessage replyMessage = new ReplyMessage("mihai@gmail.com", list, "Reminder!", date, message);
        assert (replyMessage.getOriginal().equals(message));

    }

}
