

import repository.db.FriendshipDb;
import repository.db.FriendshipRequestDb;
import repository.db.MessageDb;
import repository.db.UserDb;
import service.entityservice.*;
import service.validators.MessageValidator;
import service.validators.UserValidator;
import service.SuperService;
import tests.Tests;
import ui.SuperConsole;

public class Main {

    public static void main(String[] args) {
        Tests.RunALL();

        UserValidator userValidator= new UserValidator();
        UserDb userDb= new UserDb("jdbc:postgresql://localhost:5432/SocialNetwork","postgres","postgres");
        FriendshipDb friendshipDb= new FriendshipDb("jdbc:postgresql://localhost:5432/SocialNetwork","postgres","postgres");
        MessageDb messageDb = new MessageDb("jdbc:postgresql://localhost:5432/SocialNetwork","postgres","postgres");
        FriendshipRequestDb friendshipRequestDb = new FriendshipRequestDb("jdbc:postgresql://localhost:5432/SocialNetwork", "postgres", "postgres");
        UserService userService = new UserService(userDb, userValidator);
        FriendshipService friendshipService = new FriendshipService(friendshipRequestDb, friendshipDb);
        NetworkService networkService = new NetworkService(userDb, friendshipDb);
        MessageService messageService = new MessageService(messageDb);
        MessageValidator messageValidator= new MessageValidator(userValidator);
        SuperService service= new SuperService(messageService, networkService, friendshipService, userService,userValidator,messageValidator);
        SuperConsole console= new SuperConsole(service);
        console.run();
    }
}
